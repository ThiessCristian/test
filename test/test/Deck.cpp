#include "Deck.h"
#include <algorithm>
#include <time.h>


Deck::Deck()
{
   for (size_t i = 0; i < varietyNumber; i++) {

      Colour colour(static_cast<BaseColour>(i));

      for (size_t j = 0; j < varietyNumber; j++) {

         Shading shading = static_cast<Shading>(j);

         for (size_t k = 0; k < varietyNumber; k++) {

            Symbol symbol = static_cast<Symbol>(k);
            m_cards.push_back(Card(1, symbol, shading, colour));
            m_cards.push_back(Card(2, symbol, shading, colour));
            m_cards.push_back(Card(3, symbol, shading, colour));
         }
      }
    }
}

void Deck::shuffleDeck()
{
   srand(time(NULL));
   std::random_shuffle(m_cards.begin(), m_cards.end());
}

void Deck::printDeck(size_t numberOfCards) const
{
   for (size_t i = 0; i < numberOfCards; i++) {
      std::cout << m_cards[i];
   }
}

Card* Deck::dealCard()
{
   cardIndex++;
   return &m_cards[cardIndex - 1];
}


Deck::~Deck()
{}
