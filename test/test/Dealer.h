#pragma once
#include "Deck.h"

class Dealer
{
   Deck m_deck;

public:
   Dealer();
   ~Dealer();

   void dealCards(std::vector<Card*> cards, const size_t& nrOfCards = 12);
  
};

