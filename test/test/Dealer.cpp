#include "Dealer.h"



Dealer::Dealer():m_deck()
{}


Dealer::~Dealer()
{}

void Dealer::dealCards(std::vector<Card*> cards, const size_t & nrOfCards)
{
   for (size_t i = 0; i < nrOfCards; i++) {
      cards.push_back(m_deck.dealCard());
   }
}
