#pragma once

#include "Colour.h"
#include <iostream>

enum class Number
{
   ONE = 0,
   TWO,
   THREE,
};

enum class Symbol
{
   DIAMOND=0,
   SQUIGGLE,
   OVAL
};

enum class Shading
{
   SOLID,
   STRIPED,
   OPEN
};
class Card
{
   size_t m_number;
   Symbol m_symbol;
   Shading m_shading;
   Colour m_colour;

public:
   Card();
   Card(const size_t& number, const Symbol& symbol, const Shading& shading, const Colour& colour);
   ~Card();

   bool operator==(const Card& card) const;

   friend std::ostream& operator<<(std::ostream& o, const Card& card);

};

