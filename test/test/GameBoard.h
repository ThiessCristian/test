#pragma once

#include <vector>
#include "Player.h"
#include "Deck.h"
#include "Dealer.h"
#include "ICardCheck.h"


class GameBoard
{
   Dealer m_dealer;
   std::vector<Player> m_players;
   std::vector<Card> m_boardCards;

   const size_t maxCardsOnBoard=12;

public:
   GameBoard();
   ~GameBoard();
   void start();
};

