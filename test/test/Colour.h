#pragma once

#include <cstdint>
#include <string>

enum class BaseColour
{
   RED,
   GREEN,
   BLUE
};

class Colour
{   
   uint8_t m_red;
   uint8_t m_green;
   uint8_t m_blue;

public:
   Colour(BaseColour colour);
   Colour(const uint8_t& m_red=0, const uint8_t& m_green=0, const uint8_t& m_blue=0);
   ~Colour();

   std::string toString() const;

   bool operator==(const Colour& card) const;

};

