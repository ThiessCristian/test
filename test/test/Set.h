#pragma once
#include "Card.h"
#include <array>
class Set
{
   std::array<Card, 3> m_setCards;

public:
   Set(const Card& card1,const Card& card2,const Card& card3);

   bool validateSet();

   ~Set();
};

