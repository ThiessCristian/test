#include "Colour.h"
#include <sstream>

Colour::Colour(BaseColour colour)
{
   switch (colour) {
      case BaseColour::RED: {
         Colour(255, 0, 0);
         break;
      }
      case BaseColour::GREEN: {
         Colour(0, 255, 0);
         break;
      }
      case BaseColour::BLUE: {
         Colour(0, 0, 255);
         break;
      }
      default: {
         Colour();
         break;
      }
   }
}

Colour::Colour(const uint8_t& red, const uint8_t& green, const uint8_t& blue)
{
   m_red = red;
   m_green = green;
   m_blue = blue;
}

Colour::~Colour()
{}

std::string Colour::toString() const
{
   std::stringstream resultString;
   resultString << "(" << m_red << "." << m_green << "." << m_blue << ")";
   return resultString.str();
}

bool Colour::operator==(const Colour & card) const
{
   return m_red==card.m_red && m_green==card.m_green &&m_blue == card.m_blue;
}
