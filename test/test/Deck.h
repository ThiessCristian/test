#pragma once
#include <array>
#include <vector>
#include "Card.h"

class Deck
{
   const size_t varietyNumber = 3;
   std::vector<Card> m_cards;
   size_t cardIndex = 0;

public:
   Deck();
   ~Deck();

   void shuffleDeck();
   void printDeck(size_t nrOfCards=81) const;
   Card* dealCard();

};

