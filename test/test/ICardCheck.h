
#include "Card.h"

class ICardCheck
{
   virtual bool checkCards(const Card& card1, const Card& card2, const Card& card3) = 0;

};