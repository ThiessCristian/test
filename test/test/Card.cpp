#include "Card.h"



Card::Card()
{}

Card::Card(const size_t & number, const Symbol & symbol, const Shading & shading, const Colour & colour):
   m_number(number),m_symbol(symbol),m_shading(shading),m_colour(colour)
{}


Card::~Card()
{}

bool Card::operator==(const Card & card) const
{
   return m_number == card.m_number && m_symbol == card.m_symbol && m_shading == card.m_shading && m_colour == card.m_colour;
}

std::ostream & operator<<(std::ostream & o, const Card & card)
{
   //todo: change print
   o << card.m_number << " " << (int)card.m_symbol << " " << (int)card.m_shading << " " << card.m_colour.toString()<<std::endl;
   return o;
}
