#include "HistoryLogger.h"
#include <fstream>


HistoryLogger::HistoryLogger()
{}


HistoryLogger::~HistoryLogger()
{}

void HistoryLogger::logEvent(std::string eventText, std::string filename)
{
   std::ofstream file(filename);
   file << eventText << std::endl;
   file.close();
}


